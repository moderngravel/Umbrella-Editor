package com.moderngravel.umbrellaedit.effect;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import com.moderngravel.umbrellaedit.framework.Filter;

public class MeanBlurFilter extends Filter {

	private int size;

	public void init(Map<String, Object> params) {
		this.size = (int)(Integer.valueOf(params.get("size").toString()) / 5);
	}

	@Override
	public String getName() {
		return "Mean Blur";
	}

	@Override
	public Color filterPixel(Color col, int x, int y) {
		int sumR = 0;
		int sumG = 0;
		int sumB = 0;
		for (int i = -size; i <= size; i++) {
			for (int j = -size; j <= size; j++) {
				int xLoc = x + i;
				if (xLoc < 0 || xLoc >= width) {
					xLoc = x;
				}
				int yLoc = y + j;
				if (yLoc < 0 || yLoc >= height) {
					yLoc = y;
				}
				Color colAt = new Color(input.getRGB(xLoc, yLoc));

				sumR += colAt.getRed();
				sumG += colAt.getGreen();
				sumB += colAt.getBlue();
			}
		}
		int boxSize = (int) Math.pow(size * 2 + 1, 2);
		return new Color(sumR / boxSize, sumG / boxSize, sumB / boxSize);
	}

	@Override
	public Map<String, String> getParams() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("size", "percent");
		return params;
	}

}
