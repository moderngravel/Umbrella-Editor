package com.moderngravel.umbrellaedit.effect;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moderngravel.umbrellaedit.framework.Filter;

public class DitherQuantizeFilter extends Filter {
	private List<Color> colors;
	private int ditherAmount;

	@Override
	public String getName(){
		return "Dither-Quantize";
	}
	
	@Override
	public void init(Map<String, Object> params) {
		colors = new ArrayList<Color>();
		String raw = params.get("colors").toString();
		String[] colorListStrings = raw.split(",");
		for (int i = 0; i < colorListStrings.length; i++) {
			colorListStrings[i] = colorListStrings[i].replaceAll("#", "");
			colorListStrings[i] = colorListStrings[i].replaceAll("0x", "");
			int color = Integer.valueOf(colorListStrings[i],16);
			colors.add(new Color(color));
		}
		ditherAmount = Integer.valueOf(params.get("dither").toString());
	}
	
	@Override
	public Color filterPixel(Color col, int x, int y){
		Color closest = null;
		Color secondClosest = null;
		double bestDist = Double.MAX_VALUE;
		for (Color candidate : colors) {
			double distance = getColorEuclideanDistance(col, candidate);
			if (distance < bestDist) {
				secondClosest = closest;
				bestDist = distance;
				closest = candidate;
			}
		}

		if (secondClosest == null) {
			return closest;
		} else {
			double firstDistance = getColorEuclideanDistance(col, closest);
			
			double secondDistance = getColorEuclideanDistance(col, secondClosest);
			
			if(Math.abs(firstDistance-secondDistance)<ditherAmount && (x + y) % 2 == 0){
				return secondClosest;
			} else {
				return closest;
			}

			
		}
	}

	private double getColorEuclideanDistance(Color col1, Color col2) {
		return Math
				.sqrt(Math.pow(
						col1.getRed() - col2.getRed(),
						2)
						+ Math.pow(col1.getGreen()
								- col2.getGreen(), 2)
						+ Math.pow(col1.getBlue()
								- col2.getBlue(), 2));
	}
	
	@Override
	public Map<String, String> getParams(){
		Map<String, String> params = new HashMap<String,String>();
		params.put("colors", "colorlist");
		params.put("dither", "percent");
		return params;
	}
}
