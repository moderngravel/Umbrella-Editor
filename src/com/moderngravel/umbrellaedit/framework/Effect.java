package com.moderngravel.umbrellaedit.framework;

import java.awt.image.BufferedImage;
import java.util.Map;

import javax.swing.JFrame;

public interface Effect {

	public abstract String getName();

	public void init(Map<String, Object> params);

	public Map<String, String> getParams();

	BufferedImage getProcessedImage(BufferedImage currentImage, JFrame frame);
}