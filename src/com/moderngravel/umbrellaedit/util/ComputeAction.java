package com.moderngravel.umbrellaedit.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.concurrent.RecursiveAction;

import com.moderngravel.umbrellaedit.framework.Filter;

public class ComputeAction extends RecursiveAction {

	private BufferedImage img;
	private Filter filter;
	private int start, length;
	private int width;
	private boolean skipMultiCore;

	public ComputeAction(BufferedImage img, int start, int length,
			Filter filter, int width, boolean skip) {
		this.img = img;
		this.start = start;
		this.length = length;
		this.filter = filter;
		this.width = width;
		this.skipMultiCore = skip;
	}

	@Override
	protected void compute() {
		if (length < 100 || skipMultiCore) {
			for (int x = 0; x < width; x++) {
				for (int y = start; y < start + length; y++) {
					int col = img.getRGB(x, y);
					Color col2 = filter.filterPixel(new Color(col), x, y);
					img.setRGB(x, y, col2.getRGB());
				}
			}
		} else {
			double half = length / 2.0;
			int topSize = (int) Math.ceil(half);
			int bottomSize = (int) Math.floor(half);
			invokeAll(new ComputeAction(img, start, length - topSize, filter,
					width,false), new ComputeAction(img, start + bottomSize, length
					- bottomSize, filter, width,false));

		}
	}

	public BufferedImage getResult() {
		return img;
	}

}
